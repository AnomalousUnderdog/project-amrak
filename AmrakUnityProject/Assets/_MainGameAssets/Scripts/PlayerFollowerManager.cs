using UnityEngine;
using System.Collections;

public class PlayerFollowerManager : MonoBehaviour
{
	public static PlayerFollowerManager GetFromPlayer()
	{
		GameObject pg = GameObject.FindWithTag("Player");
		if (pg == null)
		{
			return null;
		}

		PlayerFollowerManager p = pg.GetComponent<PlayerFollowerManager>();
		return p;
	}

	[SerializeField]
	int _maxFollowers = 1;

	int _currentFollowers = 0;

	public bool HasVacancy { get{ return _currentFollowers < _maxFollowers; } }
	public bool HasNoVacancy { get{ return _currentFollowers >= _maxFollowers; } }

	public bool HasFollowers { get{ return _currentFollowers > 0; } }

	public void AddFollower()
	{
		++_currentFollowers;
	}
	public void RemoveFollower()
	{
		--_currentFollowers;
	}
}
