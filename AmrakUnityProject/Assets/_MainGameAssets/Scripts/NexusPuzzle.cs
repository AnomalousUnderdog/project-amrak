using UnityEngine;
using System.Collections;

public class NexusPuzzle : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 2; } }

	void OnEnable()
	{
		PlayerSoundController.Register(this);
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
	}

	Renderer[] _renderers;

	Color _originalColor;

	[SerializeField]
	float _beatGain = 0.25f;

	[SerializeField]
	float _decreaseRate = 0.2f;

	[SerializeField]
	Color _livenedColor;

	[SerializeField]
	float _beatActivateRange = 4.0f;

	float _value = 0.0f;
	public float Value { get{ return _value; } }

	void UpdateValueDecrease()
	{
		if (_value >= 1)
		{
			return;
		}
		_value -= _decreaseRate * Time.deltaTime;
		_value = Mathf.Max(0, _value);

		SetColor(Color.Lerp(_originalColor, _livenedColor, _value));
	}

	Color GetColor()
	{
		return _renderers[0].material.color;
	}

	void SetColor(Color c)
	{
		for (int n = 0; n < _renderers.Length; ++n)
		{
			_renderers[n].material.color = c;
		}
	}

	void Start()
	{
		_renderers = GetComponentsInChildren<Renderer>();
		_originalColor = GetColor();
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		float distSqr = _beatActivateRange * _beatActivateRange;

		Vector3 myPos = transform.position;

		if ((pos - myPos).sqrMagnitude > distSqr)
		{
			return false;
		}

		if (source == BeatSource.Player)
		{
			if (type == BeatType.Heartbeat)
			{
				_value += _beatGain;
				SetColor(Color.Lerp(_originalColor, _livenedColor, _value));
				Debug.Log("nexus puzzle ate heartbeat");

				if (_value >= 1.0f)
				{
					OnPuzzleComplete();
				}

				return true;
			}
		}
		return false;
	}

	void Update()
	{
		UpdateValueDecrease();
		UpdateWhiteBg();
	}

	[SerializeField]
	float _winWaitDuration = 1.0f;

	bool _fadeToWhiteNow = false;

	[SerializeField]
	Texture2D _whiteBg;

	[SerializeField]
	float _fadeSpeed;

	[SerializeField]
	GUISkin _guiSkin;

	float _alpha = 0.0f;

	void OnPuzzleComplete()
	{
		Invoke("Finish", _winWaitDuration);
	}

	void Finish()
	{
		_fadeToWhiteNow = true;
	}

	void UpdateWhiteBg()
	{
		if (_fadeToWhiteNow)
		{
			bool notEnd = _alpha < 1.0f;
			_alpha += _fadeSpeed * Time.deltaTime;

			if (notEnd && _alpha >= 1.0f)
			{
				Invoke("ShowMsg1", 1.0f);
				Invoke("ShowMsg2", 4.0f);
			}
		}
	}

	// this code in, 45 minutes until deadline...

	void ShowMsg1()
	{
		_msg = "PURCHASE THE UPCOMING DLC TO SEE THE REAL ENDING.";
	}

	void ShowMsg2()
	{
		_msg = "PURCHASE THE UPCOMING DLC TO SEE THE REAL ENDING. LOL.";
	}

	string _msg = "";

	void OnGUI()
	{
		if (_fadeToWhiteNow && _whiteBg != null)
		{
			GUI.skin = _guiSkin;

			Color t = GUI.color;
			t.a = _alpha;
			GUI.color = t;

			GUI.DrawTexture(new Rect(0,0,Screen.width,Screen.height), _whiteBg);

			float w = Screen.width * 0.8f;
			float h = Screen.height * 0.25f;
	
			GUI.color = Color.black;
			GUI.Label(new Rect((Screen.width - w)/2, h, w, h), _msg, "Title");
		}
	}
}
