using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BarberDoorBeatListener : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 1; } }

	[SerializeField]
	AudioClip _barberKnockSound;

	[SerializeField]
	AudioClip _doorOpenSound;

	[SerializeField]
	float _beatActivateRange = 4.0f;

	[SerializeField]
	float _fadeToLifelessSpeed = 2.0f;

	[SerializeField]
	float _listenWaitDuration = 0.75f;

	[SerializeField]
	Collider _doorCollider;

	float _lastListenTime = 0.0f;
	BeatType _beatTypeListened = BeatType.None;

	[SerializeField]
	Animation _spinAnimation;
	[SerializeField]
	Animation _doorAnimation;

	[SerializeField]
	ParticleSystem _particles;

	float _barberKnockTime;

	bool _opened = false;

	const string SPIN_ANIMATION = "ParlorSpin";
	const string OPEN_ANIMATION = "Open";

	void Start()
	{
		_spinAnimation[SPIN_ANIMATION].wrapMode = WrapMode.Loop;
		_spinAnimation.Play(SPIN_ANIMATION);
		_spinAnimation[SPIN_ANIMATION].speed = 0.0f;
		//_animation[SPIN_ANIMATION].blendMode = AnimationBlendMode.Additive;

		//_spinAnimation[SPIN_ANIMATION].layer = 1;

		_doorAnimation[OPEN_ANIMATION].wrapMode = WrapMode.ClampForever;
		_doorAnimation[OPEN_ANIMATION].speed = 0.5f;

		//_doorAnimation[OPEN_ANIMATION].blendMode = AnimationBlendMode.Additive;

		//_animation[OPEN_ANIMATION].enabled = false;
		//_animation[OPEN_ANIMATION].normalizedTime = 0;
	}

	void OnEnable()
	{
		PlayerSoundController.Register(this);
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
	}

	void PlaySound(AudioClip c)
	{
		audio.clip = c;
		audio.Play();
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		if (source != BeatSource.Player)
		{
			return false;
		}
		if (audio.isPlaying)
		{
			return false;
		}

		float distSqr = _beatActivateRange * _beatActivateRange;

		Vector3 myPos = _doorCollider.ClosestPointOnBounds(pos);

		if ((pos - myPos).sqrMagnitude > distSqr)
		{
			return false;
		}

		if (type == BeatType.HeartbeatCorrection)
		{
			_beatTypeListened = BeatType.None;
			return false;
		}

		_lastListenTime = Time.time;
		_beatTypeListened = type;

		return false;
	}

	float _lastKnockSoundTime;

	void Update()
	{
		if (audio.isPlaying && audio.clip == _barberKnockSound)
		{
			float time = Time.time - _barberKnockTime;
			
			if ((time > 0.0f && _lastKnockSoundTime == 0.0f) ||
				(time > 0.2f && _lastKnockSoundTime <= 0.2f) ||
				(time > 0.4f && _lastKnockSoundTime <= 0.4f) ||
				(time > 0.6f && _lastKnockSoundTime <= 0.6f) ||
				(time > 0.96f && _lastKnockSoundTime <= 0.96f) ||
				(time > 1.51f && _lastKnockSoundTime <= 1.51f) ||
				(time > 1.81f && _lastKnockSoundTime <= 1.81f)
			)
			{
				_particles.Emit(1);
			}
			_lastKnockSoundTime = time;
		}

		if (_lastKnockSoundTime > 1.81f)
		{
			_spinAnimation[SPIN_ANIMATION].speed = Mathf.Lerp(_spinAnimation[SPIN_ANIMATION].speed, 0.0f, _fadeToLifelessSpeed * Time.deltaTime);
		}

		if (Time.time - _lastListenTime >= _listenWaitDuration && _beatTypeListened != BeatType.None)
		{
			if (_beatTypeListened != BeatType.BarberKnock && !_opened)
			{
				PlaySound(_barberKnockSound);
				_barberKnockTime = Time.time;
				_lastKnockSoundTime = 0;
				_spinAnimation[SPIN_ANIMATION].speed = 1.0f;
			}
			else if (!_opened)//if (_beatTypeListened == BeatType.BarberKnock)
			{
				Debug.Log("open barber door");
				_doorAnimation.Play(OPEN_ANIMATION);
				PlaySound(_doorOpenSound);
				_opened = true;
			}

			_beatTypeListened = BeatType.None;
		}
	}
}
