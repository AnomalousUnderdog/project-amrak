using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkyColorBeatListener : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 1; } }

	[SerializeField]
	Color _livenedColor;

	[SerializeField]
	float _fadeToLifelessSpeed = 1.0f;

	Color _fogOriginalColor;
	Color _camBgOriginalColor;

	void OnEnable()
	{
		PlayerSoundController.Register(this);
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
	}

	void Start()
	{
		_fogOriginalColor = RenderSettings.fogColor;
		_camBgOriginalColor = Camera.main.backgroundColor;
	}
	
	void Update()
	{
		SetFogColor(Color.Lerp(GetFogColor(), _fogOriginalColor, _fadeToLifelessSpeed * Time.deltaTime));
		SetCamBgColor(Color.Lerp(GetCamBgColor(), _camBgOriginalColor, _fadeToLifelessSpeed * Time.deltaTime));
	}

	void SetCamBgColor(Color c)
	{
		Camera.main.backgroundColor = c;
	}

	Color GetCamBgColor()
	{
		return Camera.main.backgroundColor;
	}

	void SetFogColor(Color c)
	{
		RenderSettings.fogColor = c;
	}

	Color GetFogColor()
	{
		return RenderSettings.fogColor;
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		if (source != BeatSource.Player)
		{
			return false;
		}
		if (type != BeatType.Heartbeat)
		{
			return false;
		}

		SetFogColor(_livenedColor);
		SetCamBgColor(_livenedColor);
		return false;
	}
}
