using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LightColorHeartbeat : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 1; } }

	void OnEnable()
	{
		PlayerSoundController.Register(this);
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
	}

	[SerializeField]
	Color _livenedColor;

	[SerializeField]
	float _fadeToLifelessDuration = 1.0f;

	Color _originalColor;

	void Start()
	{
		_originalColor = light.color;
	}
	
	void Update()
	{
		light.color = Color.Lerp(light.color, _originalColor, _fadeToLifelessDuration * Time.deltaTime);
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		if (source != BeatSource.Player)
		{
			return false;
		}
		if (type != BeatType.Heartbeat)
		{
			return false;
		}

		light.color = _livenedColor;
		return false;
	}
}
