using UnityEngine;
using System.Collections;

public class PlayerMoveInput : MonoBehaviour
{
	RigidbodyMoveController _move;

	[SerializeField]
	float _moveSpeed = 3.0f;

	void Start()
	{
		_move = GetComponent<RigidbodyMoveController>();
	}
	
	void Update()
	{
		Vector3 nowVelocity = Vector3.zero;
		nowVelocity.x = Input.GetAxisRaw("Horizontal");
		nowVelocity.z = Input.GetAxisRaw("Vertical");

		if (nowVelocity.x != 0 && nowVelocity.z != 0)
		{
			nowVelocity *= 0.707f;
		}

		nowVelocity *= _moveSpeed;

		_move.SetVelocity(nowVelocity);
		if (nowVelocity.x != 0 || nowVelocity.z != 0)
		{
			_move.SetFacingDirection(nowVelocity.normalized);
		}
	}
}
