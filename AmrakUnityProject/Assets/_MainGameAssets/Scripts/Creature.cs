using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Creature : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 0; } }

	[SerializeField]
	Color _livenedColor;

	Color _originalColor;

	[SerializeField]
	float _fadeToLifelessSpeed = 2.0f;

	[SerializeField]
	AudioClip _sound;

	[SerializeField]
	float _beatActivateRange = 4.0f;

	[SerializeField]
	float _listenWaitDuration = 0.75f;

	[SerializeField]
	ParticleSystem _soundParticles;

	[SerializeField]
	float _moveSpeed = 3.0f;

	[SerializeField]
	float _minDistanceWhileFollowing = 0.5f;

	[SerializeField]
	BeatSource _sourceType;

	float _lastListenTime = 0.0f;
	BeatType _beatTypeListened = BeatType.None;

	Renderer _renderer;

	bool _isEnlivened = false;

	bool IsEnlivened { get{ return _isEnlivened; } set{ _isEnlivened = value; } }

	RecordedSound[] _soundsToRepeat;
	int _currentRepeatSoundIdx = -1;
	float _lastRepeatSoundTime = 0;

	PlayerFollowerManager _player;

	RigidbodyMoveController _move;

	void Awake()
	{
		_renderer = GetComponentInChildren<Renderer>();
		_originalColor = GetColor();
		_player = PlayerFollowerManager.GetFromPlayer();
		_move = GetComponent<RigidbodyMoveController>();
	}

	void OnEnable()
	{
		PlayerSoundController.Register(this);
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
	}




	List<IPlayerBeatListener> _hearbeatListeners = new List<IPlayerBeatListener>();

	public void AddBeatListener(IPlayerBeatListener l)
	{
		_hearbeatListeners.Add(l);
	}

	public void RemoveBeatListener(IPlayerBeatListener l)
	{
		_hearbeatListeners.Remove(l);
	}

	public static void Register(IPlayerBeatListener l, string tag)
	{
		GameObject pg = GameObject.FindWithTag(tag);
		if (pg == null)
		{
			return;
		}

		Creature p = pg.GetComponent<Creature>();

		p.AddBeatListener(l);
	}

	public static void Unregister(IPlayerBeatListener l, string tag)
	{
		GameObject pg = GameObject.FindWithTag(tag);
		if (pg == null)
		{
			return;
		}

		Creature p = pg.GetComponent<Creature>();

		p.RemoveBeatListener(l);
	}

	void SignalBeatToListeners(BeatType type)
	{
		for (int n = 0; n < _hearbeatListeners.Count; ++n)
		{
			_hearbeatListeners[n].OnBeat(transform.position, _sourceType, type, _soundsToRepeat);
		}
	}




	protected void PlaySound(AudioClip c)
	{
		audio.clip = c;
		audio.Play();
		_soundParticles.Emit(1);
	}

	protected void PlaySound()
	{
		audio.pitch = 1;
		PlaySound(_sound);
	}

	protected void PlaySound(float pitch)
	{
		audio.pitch = Mathf.Max(pitch, 0.9f);
		PlaySound(_sound);
	}

	const string COLOR_NAME = "_Color";

	void SetColor(Color c)
	{
		_renderer.material.SetColor(COLOR_NAME, c);
	}

	Color GetColor()
	{
		return _renderer.material.GetColor(COLOR_NAME);
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		if (IsEnlivened)
		{
			_soundsToRepeat = sounds;
			_currentRepeatSoundIdx = -1;
			//Debug.Log("_soundsToRepeat.Length: " + _soundsToRepeat.Length);
			_lastListenTime = Time.time;
			_beatTypeListened = type;
			Debug.Log(_sourceType + " going to repeat " + _beatTypeListened);
			return false;
		}

		_soundsToRepeat = null;

		if (audio.isPlaying)
		{
			return false;
		}

		if (type != BeatType.Heartbeat)
		{
			return false;
		}

		float distSqr = _beatActivateRange * _beatActivateRange;

		Vector3 myPos = collider.ClosestPointOnBounds(pos);

		if ((pos - myPos).sqrMagnitude > distSqr)
		{
			return false;
		}

		SetColor(_livenedColor);
		_lastListenTime = Time.time;
		_beatTypeListened = type;

		return false;
	}

	void UpdateFollow()
	{
		if (IsEnlivened)
		{
			Vector3 vel = _player.transform.position - transform.position;

			float distSqr = vel.sqrMagnitude;

			vel.Normalize();
			_move.SetFacingDirection(vel);

			if (distSqr > _minDistanceWhileFollowing*_minDistanceWhileFollowing)
			{
				vel *= _moveSpeed;
				_move.SetVelocity(vel);
			}
			else
			{
				_move.SetVelocity(Vector3.zero);
			}
		}
		else
		{
			_move.SetVelocity(Vector3.zero);
		}
	}

	void Update()
	{
		if (Time.time - _lastListenTime >= _listenWaitDuration) 
		{
			if (IsEnlivened && _currentRepeatSoundIdx < 0)
			{
				// start playback of sounds
				_currentRepeatSoundIdx = 0;
				_lastRepeatSoundTime = Time.time;
			}
			else if (_beatTypeListened == BeatType.Heartbeat)
			{
				Enliven();
			}
		}
		UpdateRepeatSounds();
		UpdateFollow();
	}

	float _lastRepeatSoundTimeCheck = 0f;

	void UpdateRepeatSounds()
	{
		if (_currentRepeatSoundIdx < 0)
		{
			return;
		}
		if (_soundsToRepeat == null)
		{
			return;
		}

		if (_currentRepeatSoundIdx == 0)
		{
			PlaySound(_soundsToRepeat[0].Pitch);
			++_currentRepeatSoundIdx;
			_lastRepeatSoundTime = Time.time;
		}
		else if (Time.time - _lastRepeatSoundTime >= _soundsToRepeat[_currentRepeatSoundIdx].Time && _lastRepeatSoundTimeCheck < _soundsToRepeat[_currentRepeatSoundIdx].Time)
		{
			PlaySound(_soundsToRepeat[_currentRepeatSoundIdx].Pitch);
			++_currentRepeatSoundIdx;
			_lastRepeatSoundTime = Time.time;
			_lastRepeatSoundTimeCheck = Time.time - _lastRepeatSoundTime;
		}
		if (_currentRepeatSoundIdx >= _soundsToRepeat.Length)
		{
			Debug.Log("signaling to creature listeners...");
			SignalBeatToListeners(_beatTypeListened);

			_currentRepeatSoundIdx = -1;
			_soundsToRepeat = null;
		}
	}

	void Enliven()
	{
		if (IsEnlivened)
		{
			return;
		}
		_player.AddFollower();
		IsEnlivened = true;
		PlaySound();
		rigidbody.isKinematic = false;
	}
}
