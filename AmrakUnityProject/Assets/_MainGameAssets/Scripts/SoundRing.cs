using UnityEngine;
using System.Collections;

public class SoundRing : MonoBehaviour
{
	Animation _anim;
	Renderer _renderer;

	const string PLAY_ANIMATION_NAME = "Play";
	const string MATERIAL_COLOR_NAME = "_TintColor";

	void Awake()
	{
		_anim = GetComponentInChildren<Animation>();
		_renderer = GetComponentInChildren<Renderer>();

		_anim[PLAY_ANIMATION_NAME].wrapMode = WrapMode.ClampForever;
	}

	float GetAlpha()
	{
		return _renderer.material.GetColor(MATERIAL_COLOR_NAME).a;
	}

	void SetAlpha(float a)
	{
		Color currentColor = _renderer.material.GetColor(MATERIAL_COLOR_NAME);
		currentColor.a = a;
		_renderer.material.SetColor(MATERIAL_COLOR_NAME, currentColor);
	}

	public void	Play()
	{
		_anim.Play(PLAY_ANIMATION_NAME);
	}

	void Update()
	{
		if (_anim[PLAY_ANIMATION_NAME].normalizedTime >= 1.0f)
		{
			Destroy(gameObject);
		}
		if (_anim.isPlaying)
		{
			SetAlpha(Mathf.Lerp(GetAlpha(), 0, _anim[PLAY_ANIMATION_NAME].normalizedTime));
		}
	}
}
