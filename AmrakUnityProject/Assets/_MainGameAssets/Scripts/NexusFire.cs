using UnityEngine;
using System.Collections;
using System.Collections.Generic;

// quick and dirty hack... duplicate of ParticleBeatListener... ~3 hours to deadline...

public class NexusFire : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 3; } }

	[SerializeField]
	float _fadeToLifelessDuration = 1.0f;

	[SerializeField]
	float _fadeToLifelessColorDuration = 1.0f;

	[SerializeField]
	float _beatActivateRange = 4.0f;

	ParticleSystem _particles;

	[SerializeField]
	NexusPuzzle _nexusPuzzle;

	void OnEnable()
	{
		PlayerSoundController.Register(this);
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
	}

	void Start()
	{
		_particles = GetComponent<ParticleSystem>();
		SetSaturation(0.0f);
		SetPlaybackSpeed(0.0f);
	}

	void Update()
	{
		if (_nexusPuzzle.Value < 1.0)
		{
			SetSaturation(Mathf.Lerp(GetSaturation(), 0, _fadeToLifelessColorDuration * Time.deltaTime));
		
			SetPlaybackSpeed(Mathf.Lerp(GetPlaybackSpeed(), 0, _fadeToLifelessDuration * Time.deltaTime));
		}
	}

	const string SATURATION_NAME = "_Saturation";

	void SetSaturation(float s)
	{
		renderer.material.SetFloat(SATURATION_NAME, s);
	}

	float GetSaturation()
	{
		return renderer.material.GetFloat(SATURATION_NAME);
	}

	void SetPlaybackSpeed(float s)
	{
		_particles.playbackSpeed = s;
	}

	float GetPlaybackSpeed()
	{
		return _particles.playbackSpeed;
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		if (source != BeatSource.Player)
		{
			return false;
		}
		if (type != BeatType.Heartbeat)
		{
			return false;
		}

		float distSqr = _beatActivateRange * _beatActivateRange;

		if ((pos - transform.position).sqrMagnitude <= distSqr)
		{
			SetSaturation(1.0f);
			SetPlaybackSpeed(1.0f);
		}
		return false;
	}
}
