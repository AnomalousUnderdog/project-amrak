using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class IntroHandler : MonoBehaviour
{
	[SerializeField]
	GUISkin _guiSkin;

	[SerializeField]
	float _waitDuration;

	[SerializeField]
	float _fadeSpeed;

	float _timeToFadeStart;

	float _alpha = 1.0f;

	void Start()
	{
		_timeToFadeStart = Time.time + _waitDuration;
	}
	
	void Update()
	{
		if (Time.time >= _timeToFadeStart)
		{
			_alpha -= _fadeSpeed * Time.deltaTime;
		}
	}

	void OnGUI()
	{
		GUI.skin = _guiSkin;

		Color t = GUI.color;
		t.a = _alpha;
		GUI.color = t;

		float w = Screen.width * 0.8f;
		float h = Screen.height * 0.25f;

		GUI.Label(new Rect((Screen.width - w)/2, h, w, h), "BRING BACK LIFE INTO THIS FORSAKEN WORLD, STRANGER.", "Title");
	}
}
