using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class SoundAsset : ScriptableObject
{
	[SerializeField]
	AudioClip _sound;

	[SerializeField]
	SoundAsset _listensTo;

#if UNITY_EDITOR
	[MenuItem("Assets/Create/Sound Asset")]
	static void CreateNewFile()
	{
		string filename = AssetUtility.CreateNewFilename("NewSoundAsset", ".SoundAsset.asset");

		AssetDatabase.CreateAsset(ScriptableObject.CreateInstance<SoundAsset>(), filename);
		
		AssetDatabase.ImportAsset(filename, ImportAssetOptions.ForceUpdate);
		Selection.activeObject = AssetDatabase.LoadAssetAtPath(filename, typeof(TextAsset));
		EditorUtility.FocusProjectWindow();
	}
#endif
}
