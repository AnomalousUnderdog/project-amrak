using UnityEngine;
using System.Collections;

//
// replacement for CharacterController
//

public class RigidbodyMoveController : MonoBehaviour
{
	//Vector3 _lastMovementDirection;

	// The direction the character wants to face towards, in world space.
	Vector3 _facingDirection;

	Vector3 _velocity;

	[SerializeField]
	float _walkingSnappyness = 50;

	Quaternion _targetRotation;

	[SerializeField]
	float _turnSpeed = 5.0f;

	//[HideInInspector]
	//public bool isGrounded = true;

	public void SetFacingDirection(Vector3 newDir, float newTurnSpeed = 0.0f)
	{
		_facingDirection = newDir;
		if (newTurnSpeed != 0.0f)
		{
			_turnSpeed = newTurnSpeed;
		}
	}

	public void SetVelocity(Vector3 velocity)
	{
		_velocity = velocity;

		//Vector3 direction = velocity.normalized;
		//if (direction != Vector3.zero)
		//{
		//	_lastMovementDirection = direction;
		//}
	}

	[SerializeField]
	LayerMask _isGroundedCheckLayer;


	void FixedUpdate()
	{
		// Handle the movement of the character
		Vector3 deltaVelocity = _velocity - rigidbody.velocity;
		if (rigidbody.useGravity)
		{
			deltaVelocity.y = 0;
		}
		//if (deltaVelocity != Vector3.zero && deltaVelocity.x != Mathf.Infinity && deltaVelocity.x != -Mathf.Infinity && deltaVelocity.z != Mathf.Infinity && deltaVelocity.z != -Mathf.Infinity)
		{
			deltaVelocity *= _walkingSnappyness;
			//Debug.Log("deltaVelocity: " + deltaVelocity.ToString("0.000"));
			rigidbody.AddForce(deltaVelocity, ForceMode.Acceleration);
		}

		// Setup player to face facingDirection, or if that is zero, then the movementDirection
		Vector3 faceDir = _facingDirection;

		if (faceDir != Vector3.zero)
		{
			_targetRotation = Quaternion.LookRotation(faceDir);
			//Debug.Log("rigidbody.rotation before: " + rigidbody.rotation.ToString("0.000"));

			Quaternion newRotation = Quaternion.RotateTowards(rigidbody.rotation, _targetRotation, _turnSpeed * Time.deltaTime);

			rigidbody.MoveRotation(newRotation);

			//Debug.Log("_targetRotation: " + _targetRotation.ToString("0.000"));
			//Debug.Log("newRotation: " + newRotation.ToString("0.000"));
			//Debug.Log("rigidbody.rotation: " + rigidbody.rotation.ToString("0.000"));
		}
	}
}
