using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WindZoneHeartbeatListener : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 1; } }

	void OnEnable()
	{
		PlayerSoundController.Register(this);
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		if (source != BeatSource.Player)
		{
			return false;
		}
		if (type != BeatType.Heartbeat)
		{
			return false;
		}

		animation.Play("WindMain5to0");
		return false;
	}
}
