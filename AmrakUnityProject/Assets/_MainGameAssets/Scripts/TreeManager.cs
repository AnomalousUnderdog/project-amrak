using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TreeManager : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 1; } }

	Terrain _terrain;

	[SerializeField]
	Color _treeLifelessColor;

	[SerializeField]
	Color _treeLivenedColor = Color.white;

	[SerializeField]
	AudioClip _treeLivenedSound;

	[SerializeField]
	float _soundVolume = 1.0f;

	[SerializeField]
	float _fadeDurationToLifeless = 1.0f;

	// Use this for initialization
	void Start()
	{
		_terrain = GetComponent<Terrain>();
		InitTrees();

	}

	void OnEnable()
	{
		PlayerSoundController.Register(this);
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
	}

	void InitTrees()
	{
		TreeInstance[] trees = _terrain.terrainData.treeInstances;
		for (int n = 0; n < trees.Length; ++n)
		{
			trees[n].color = _treeLifelessColor;
		}

		_terrain.terrainData.treeInstances = trees;
	}

	void UpdateTrees()
	{
		TreeInstance[] trees = _terrain.terrainData.treeInstances;
		for (int n = 0; n < trees.Length; ++n)
		{
			trees[n].color = Color.Lerp(trees[n].color, _treeLifelessColor, _fadeDurationToLifeless * Time.deltaTime);
		}

		_terrain.terrainData.treeInstances = trees;
	}

	void ColorTrees(Vector3 sourcePos, float length, Color newColor)
	{
		//length *= length; // square the length
		bool atLeastOne = false;

		TreeInstance[] trees = _terrain.terrainData.treeInstances;
		for (int n = 0; n < trees.Length; ++n)
		{
			Vector3 treePos = Vector3.Scale(trees[n].position, _terrain.terrainData.size) + transform.position;

			//Vector3 treePos = trees[n].position + transform.position;

			//Debug.Log(n + " pos: " + treePos.ToString("0.000"));
			float sqrDistanceToSource = (treePos - sourcePos).magnitude;
			//Debug.Log(sqrDistanceToSource + " <= " + length);
			if (sqrDistanceToSource <= length)
			{
				trees[n].color = newColor;
				atLeastOne = true;
			}
		}

		_terrain.terrainData.treeInstances = trees;

		if (atLeastOne && _treeLivenedSound != null)
		{
			AudioSource.PlayClipAtPoint(_treeLivenedSound, Vector3.zero, _soundVolume);
		}
	}

	void Update()
	{
		UpdateTrees();
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		if (source != BeatSource.Player)
		{
			return false;
		}
		if (type != BeatType.Heartbeat)
		{
			return false;
		}

		//Debug.Log("tree manager got heartbeat");
		ColorTrees(pos, 20, _treeLivenedColor);
		return false;
	}
}
