using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface IPlayerBeatListener
{
	bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds);
	int Priority { get; }
}

public enum BeatType
{
	None,
	SingleBeat,
	Heartbeat,
	HeartbeatCorrection,
	BarberKnock
}

public enum BeatSource
{
	None,
	Player,
	Cat,
	Dog
}

public struct RecordedSound
{
	public float Time;
	public float Pitch;
}

public class PlayerSoundController : MonoBehaviour
{
	[SerializeField]
	AudioClip _testSound;

	[SerializeField]
	SoundRing _soundRingPrefab;

	[SerializeField]
	Transform _soundRingSpawnPoint;

	List<RecordedSound> _recordedSounds = new List<RecordedSound>();
	bool _recordedSoundsUsed = false;

	float _lastRecordedSoundTime = 0.0f;
	const float RECORDED_SOUNDS_EXPIRY_DURATION = 0.75f;

	List<IPlayerBeatListener> _hearbeatListeners = new List<IPlayerBeatListener>();

	public void AddHearbeatListener(IPlayerBeatListener l)
	{
		_hearbeatListeners.Add(l);


		_hearbeatListeners.Sort(delegate(IPlayerBeatListener p1, IPlayerBeatListener p2)
		{
			if (p1.Priority == p2.Priority) return 0;
			if (p1.Priority > p2.Priority) return -1;
			if (p1.Priority < p2.Priority) return 1;
			return 0;
		});
	}

	public void RemoveHearbeatListener(IPlayerBeatListener l)
	{
		_hearbeatListeners.Remove(l);
	}

	public static void Register(IPlayerBeatListener l)
	{
		GameObject pg = GameObject.FindWithTag("Player");
		if (pg == null)
		{
			return;
		}

		PlayerSoundController p = pg.GetComponent<PlayerSoundController>();

		p.AddHearbeatListener(l);
	}

	public static void Unregister(IPlayerBeatListener l)
	{
		GameObject pg = GameObject.FindWithTag("Player");
		if (pg == null)
		{
			return;
		}

		PlayerSoundController p = pg.GetComponent<PlayerSoundController>();

		p.RemoveHearbeatListener(l);
	}

	void SignalBeatToListeners(BeatType type)
	{
		_recordedSoundsUsed = true;
		RecordedSound[] r = _recordedSounds.ToArray();
		bool abort = false;
		System.Type typeAborter = null;
		for (int n = 0; n < _hearbeatListeners.Count; ++n)
		{
			if (abort && typeAborter != _hearbeatListeners[n].GetType())
			{
				Debug.Log("now abort on " + _hearbeatListeners[n].GetType().ToString());
				break;
			}

			abort = _hearbeatListeners[n].OnBeat(transform.position, BeatSource.Player, type, r);
			if (abort)
			{
				if (typeAborter == null)
				{
					typeAborter = _hearbeatListeners[n].GetType();
					Debug.Log("signal player beat aborted by " + _hearbeatListeners[n].GetType().ToString());
				}
			}
		}
	}

	void Start()
	{
	}
	
	void Update()
	{
		if (Input.GetButtonDown("Fire1") && CanEmit())
		{
			EmitSound();
		}
		UpdateRecordSounds();
	}

	bool CanEmit()
	{
		return !audio.isPlaying || GetSoundNormalizedTime() >= 0.25f;
	}

	float GetSoundNormalizedTime()
	{
		return audio.time/audio.clip.length;
	}

	void EmitSound()
	{
		PlaySound(_testSound);
		SoundRing s = Instantiate(_soundRingPrefab, _soundRingSpawnPoint.position, _soundRingSpawnPoint.rotation) as SoundRing;
		s.Play();

		RecordSound();
	}

	void PlaySound(AudioClip a)
	{
		if (IsAlmostBarbersKnock())
		{
			audio.pitch = 1.5f;
		}
		else if (IsEvenOrderedSound)
		{
			audio.pitch = 1.0f;
		}
		else
		{
			audio.pitch = 0.5f;
		}
		audio.clip = a;
		audio.Play();
	}

	bool IsEvenOrderedSound { get{ return (_recordedSounds.Count % 2) == 0; } }

	void RecordSound()
	{
		RecordedSound s;
		if (_recordedSounds.Count == 0)
		{
			s.Time = Time.time;
			s.Pitch = audio.pitch;
			_recordedSounds.Add(s);
		}
		else
		{
			s.Time = Time.time - _lastRecordedSoundTime;
			s.Pitch = audio.pitch;
			_recordedSounds.Add(s);
		}

		_lastRecordedSoundTime = Time.time;

		Debug.Log(_recordedSounds.Count-1 + ": " + _recordedSounds[_recordedSounds.Count-1].Time.ToString("0.000"));

		//CheckSingleBeat();
		CheckProperHeartbeat();
		CheckBarbersKnock();
	}

	void CheckSingleBeatAtEnd()
	{
		if (_recordedSounds.Count == 1)
		{
			SignalBeatToListeners(BeatType.SingleBeat);
		}
		else if (_recordedSounds.Count > 0 && !_recordedSoundsUsed)
		{
			Debug.Log("signaling none");
			SignalBeatToListeners(BeatType.None);
		}
	}

	const float MIN_HEARTBEAT = 0.001f;
	const float MAX_HEARTBEAT = 0.6f;

	void CheckProperHeartbeat()
	{
		if (_recordedSounds.Count <= 1)
		{
			return;
		}

		if (IsAlmostBarbersKnock())
		{
			return;
		}

		if (_recordedSounds.Count >= 3 &&
			(_recordedSounds[1].Time >= 0.2f && _recordedSounds[1].Time <= 0.35f) &&
			(_recordedSounds[2].Time >= 0.1f)
		)
		{
			SignalBeatToListeners(BeatType.HeartbeatCorrection);
		}

		float lastlastSound = _recordedSounds[_recordedSounds.Count-2].Time;
		float lastSound = _recordedSounds[_recordedSounds.Count-1].Time;

		if (
			(_recordedSounds.Count > 2 && (lastSound >= MIN_HEARTBEAT && lastSound <= MAX_HEARTBEAT) && (lastlastSound >= MIN_HEARTBEAT)) ||
			(_recordedSounds.Count == 2 && (lastSound >= MIN_HEARTBEAT && lastSound <= MAX_HEARTBEAT))
		)
		{
			//Debug.Log("proper hearbeat");
			SignalBeatToListeners(BeatType.Heartbeat);
			//_recordedSounds.Clear();
		}
	}

	const float BARBER_KNOCK_2_MIN = 0.2f;
	const float BARBER_KNOCK_2_MAX = 0.5f;

	const float BARBER_KNOCK_3_MIN = 0.1f;
	const float BARBER_KNOCK_3_MAX = 0.5f;

	const float BARBER_KNOCK_4_MIN = 0.1f;
	const float BARBER_KNOCK_4_MAX = 0.5f;

	const float BARBER_KNOCK_5_MIN = 0.1f;
	const float BARBER_KNOCK_5_MAX = 0.5f;


	const float BARBER_KNOCK_6_MIN = 0.4f;
	const float BARBER_KNOCK_6_MAX = 0.9f;

	const float BARBER_KNOCK_7_MIN = 0.1f;
	const float BARBER_KNOCK_7_MAX = 0.5f;

	bool IsAlmostBarbersKnock()
	{
		return (_recordedSounds.Count >= 5 &&
			(_recordedSounds[1].Time >= BARBER_KNOCK_2_MIN && _recordedSounds[1].Time <= BARBER_KNOCK_2_MAX) &&
			(_recordedSounds[2].Time >= BARBER_KNOCK_3_MIN && _recordedSounds[2].Time <= BARBER_KNOCK_3_MAX) &&
			(_recordedSounds[3].Time >= BARBER_KNOCK_4_MIN && _recordedSounds[3].Time <= BARBER_KNOCK_4_MAX) &&
			(_recordedSounds[4].Time >= BARBER_KNOCK_5_MIN && _recordedSounds[4].Time <= BARBER_KNOCK_5_MAX));
	}

	void CheckBarbersKnock()
	{
		if (_recordedSounds.Count == 7 &&
			IsAlmostBarbersKnock() &&
			(_recordedSounds[5].Time >= BARBER_KNOCK_6_MIN && _recordedSounds[5].Time <= BARBER_KNOCK_6_MAX) &&
			(_recordedSounds[6].Time >= BARBER_KNOCK_7_MIN && _recordedSounds[6].Time <= BARBER_KNOCK_7_MAX)
		)
		{
			SignalBeatToListeners(BeatType.BarberKnock);
		}
	}

	void UpdateRecordSounds()
	{
		if (_recordedSounds.Count > 0 && (Time.time - _lastRecordedSoundTime >= RECORDED_SOUNDS_EXPIRY_DURATION))
		{
			CheckSingleBeatAtEnd();
			_recordedSounds.Clear();
			_recordedSoundsUsed = false;
			Debug.Log("Clear");
		}
	}
}
