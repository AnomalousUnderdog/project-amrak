using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CatGate : MonoBehaviour, IPlayerBeatListener
{
	public int Priority { get{ return 1; } }

	[SerializeField]
	Color _livenedColor;

	Color _originalColor;

	[SerializeField]
	float _fadeToLifelessSpeed = 2.0f;

	[SerializeField]
	AudioClip _sound;

	[SerializeField]
	float _beatActivateRange = 4.0f;

	[SerializeField]
	float _listenWaitDuration = 0.75f;

	float _lastListenTime = 0.0f;
	BeatType _beatTypeListened = BeatType.None;
	BeatSource _beatSourceListened = BeatSource.None;

	Renderer _renderer;
	PlayerFollowerManager _playerFollowers;

	Animation _animation;
	const string OPEN_ANIMATION_NAME = "CatGateOpen";

	void Awake()
	{
		_renderer = GetComponentInChildren<Renderer>();
		_originalColor = GetColor();
		_playerFollowers = PlayerFollowerManager.GetFromPlayer();
		_animation = GetComponent<Animation>();
	}

	void OnEnable()
	{
		PlayerSoundController.Register(this);
		Creature.Register(this, "Cat");
	}

	void OnDisable()
	{
		PlayerSoundController.Unregister(this);
		Creature.Unregister(this, "Cat");
	}

	void PlaySound(AudioClip c)
	{
		audio.clip = c;
		audio.Play();
	}

	const string COLOR_NAME = "_Color";

	void SetColor(Color c)
	{
		_renderer.material.SetColor(COLOR_NAME, c);
	}

	Color GetColor()
	{
		return _renderer.material.GetColor(COLOR_NAME);
	}

	public bool OnBeat(Vector3 pos, BeatSource source, BeatType type, RecordedSound[] sounds)
	{
		if (audio.isPlaying)
		{
			return false;
		}

		float distSqr = _beatActivateRange * _beatActivateRange;

		Vector3 myPos = collider.ClosestPointOnBounds(pos);

		if ((pos - myPos).sqrMagnitude > distSqr)
		{
			return false;
		}

		if (type == BeatType.Heartbeat || source == BeatSource.Cat)
		{
			SetColor(_livenedColor);
		}

		_lastListenTime = Time.time;
		_beatTypeListened = type;
		_beatSourceListened = source;

		Debug.Log("got " + _beatTypeListened + " from " + _beatSourceListened);

		return false;
	}

	bool _inOpenState = false;

	void Update()
	{
		if (Time.time - _lastListenTime >= _listenWaitDuration && _lastListenTime > 0) 
		{

			if (_beatSourceListened == BeatSource.Player)
			{
				_lastListenTime = -1;

				if (!_playerFollowers.HasFollowers && _beatTypeListened != BeatType.None)
				{
					PlaySound(_sound);
				}

				// fade back to grey when sound finished
				//if (!audio.isPlaying)
				//{
				//}
			}
			else if (_beatSourceListened == BeatSource.Cat)
			{
				_lastListenTime = -1;

				if (_beatTypeListened == BeatType.Heartbeat)
				{
					if (!_inOpenState)
					{
						Debug.Log("opening");
						_inOpenState = true;
						_animation.Play(OPEN_ANIMATION_NAME);
						_animation[OPEN_ANIMATION_NAME].speed = 1;
					}
					else
					{
						Debug.Log("closing");
						_inOpenState = false;
						_animation.Play(OPEN_ANIMATION_NAME);
						_animation[OPEN_ANIMATION_NAME].normalizedTime = 1;
						_animation[OPEN_ANIMATION_NAME].speed = -1;
					}
				}
				// rumble if single beat
				// open if heart beat
				PlaySound(_sound);
			}


		}

		if (_beatSourceListened == BeatSource.Cat && _beatTypeListened == BeatType.Heartbeat)
		{
		}
		else
		{
			SetColor(Color.Lerp(GetColor(), _originalColor, _fadeToLifelessSpeed * Time.deltaTime));
		}
	}
}
