using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
	[SerializeField]
	Rigidbody _player;

	[SerializeField]
	Rigidbody _camera;

	[SerializeField]
	float _followSpeed = 3.0f;

	Vector3 _offsetToPlayer;

	void Start()
	{
		_offsetToPlayer = _camera.position - _player.position;
	}
	
	void FixedUpdate()
	{
		Vector3 targetPos = _player.position + _offsetToPlayer;

		Vector3 nowPos = Vector3.Lerp(_camera.position, targetPos, _followSpeed * Time.deltaTime);

		_camera.MovePosition(nowPos);
	}
}
