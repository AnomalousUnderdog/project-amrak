using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public static class AssetUtility
{
	public static bool IsFolder(string path)
	{
		return ((System.IO.File.GetAttributes(path) & System.IO.FileAttributes.Directory) == System.IO.FileAttributes.Directory);
	}

	public static string GetFolderPath(string path)
	{
		if (!string.IsNullOrEmpty(path))
		{
			if (IsFolder(path))
			{
				return path;
			}
			else
			{
				return System.IO.Path.GetDirectoryName(path);
			}
		}

		return path;
	}

#if UNITY_EDITOR
	public static string GetSelectedAssetPath()
	{
		string tryPath = AssetDatabase.GetAssetPath(Selection.activeObject);
		return GetFolderPath(tryPath);
	}

	//
	public static string CreateNewFilename(string defaultFilename, string extensionName)
	{
		string folderPath = GetSelectedAssetPath();
		string filename = folderPath + "/" + defaultFilename + extensionName;

		int n = 0;

		string projectPath = System.IO.Path.GetDirectoryName(Application.dataPath);
		System.IO.Directory.SetCurrentDirectory(projectPath);
		while (System.IO.File.Exists(filename)) // file already exists
		{
			++n;
			filename = folderPath + "/" + defaultFilename + "." + n.ToString("000") + extensionName;
		}

		return filename;
	}
#endif
}
